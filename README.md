# extractor

## A QT base Self Extacting Executable

This application was designed specifically to be uses as a standalone statically linked binary on Mac OSX.

If run without options it just display a dialog with an indeterminate progressbar until the user closes it.

### Options
* -h, --help            Displays this help.
* -v, --version         Displays version information.
* -T, --title <title>   Window Title
* -t, --timeout <#>     Time-Out in seconds
* -a, --archive <path>  Archive Path
* -o, --out <path>      Out Path
* -s, --silent          Silent
* --url <url>           Archive URL
* --sfx <path>          Self-Extracting Executible Path
** Requires:  Archive Path (-a <path>) or Archive URL (--url <url>)
* --dsfx                Dump Self-Extracting Executible
* -x, --execute <path>  Executible Path
* --xarg <string>       Executible Arguments
* --xwait               Wait for Execution to finish
* --license             Display license information.

### Source
- Git: `https://bitbucket.org/turningtech/indeterminate-progressbar.git`


### Dependencies
- QT 5.10 (only tested staticly linked) http://download.qt.io/official_releases/qt/5.10/5.10.0/single/qt-everywhere-src-5.10.0.tar.xz
- QT 5.12.3 (only tested staticly linked)
http://download.qt.io/official_releases/qt/5.12/5.12.3/single/qt-everywhere-src-5.12.3.tar.xz

### Compile
* Build QT 5.10 from source for Static Linking:  http://doc.qt.io/qt-5/osx-deployment.html

 > ./configure -static -prefix $PWD/qtbase -opensource -confirm-license -debug-and-release \
-nomake examples -nomake tests -optimize-size -optimized-qmake -strip \
-opengl -qt-libpng -qt-zlib -qt-pcre -qt-harfbuzz -qt-freetype \
-no-openssl -no-libjpeg -no-gif -no-cups -no-iconv -no-qdbus -no-xcb \
-appstore-compliant  \
-skip qt3d \
-skip qtactiveqt \
-skip qtandroidextras \
-skip qtcanvas3d \
-skip qtcharts \
-skip qtconnectivity \
-skip qtdatavis3d \
-skip qtdeclarative \
-skip qtdoc \
-skip qtgamepad \
-skip qtgraphicaleffects \
-skip qtimageformats \
-skip qtlocation \
-skip qtmacextras \
-skip qtmultimedia \
-skip qtnetworkauth \
-skip qtpurchasing \
-skip qtquickcontrols \
-skip qtquickcontrols2 \
-skip qtremoteobjects \
-skip qtscript \
-skip qtscxml \
-skip qtsensors \
-skip qtserialbus \
-skip qtserialport \
-skip qtspeech \
-skip qtsvg \
-skip qttools \
-skip qttranslations \
-skip qtvirtualkeyboard \
-skip qtwayland \
-skip qtwebchannel \
-skip qtwebengine \
-skip qtwebglplugin \
-skip qtwebsockets \
-skip qtwebview \
-skip qtwinextras \
-skip qtx11extras \
-skip qtxmlpatterns

 > make -j4

* Use qmake that was built to generate a Makefile, i.e. .../qt-everywhere-src-5.10.0/qtbase/bin/qmake

 > qmake Progress.pro -r -spec macx-clang CONFIG+=release CONFIG+=x86_64 LIBS+=-dead_strip
 > make

or

> /Volumes/T3/qt-everywhere-src-5.12.3/qtbase/bin/qmake -spec macx-clang CONFIG+=release CONFIG+=x86_64 LIBS+=-dead_strip -r extractor.pro
> CPATH="/opt/local/include" make


### License
==============================================================================

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the
   Free Software Foundation, Inc.,
   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.

   Or visit http://www.gnu.org/licenses/

==============================================================================
