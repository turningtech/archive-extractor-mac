/*
    Copyright (C) 2016 TurningTechnologies, LLC.
    Contact: https://www.turningtechnologies.com/about-turning-technologies/contact-information

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.h"
#include <QFile>
#include <QFileDialog>

using namespace QtConcurrent;

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    QApplication::setApplicationName("Extractor");
    QApplication::setApplicationVersion(VERSION);

    QString sTitle = QApplication::applicationName();
    QString sArchiveURL = "";
    QString sArchivePath = "";
    QString sSfxPath = "";
    QString sOutPath = QApplication::applicationDirPath();
    QString sAppPath = QApplication::applicationFilePath();
    QString sExecutePath = "";
    QString sExecuteRelativePath = "";
    QString sExecuteArgs = "";
    QString sMoveFromPath = "";
    QString sMoveToPath = "";
    bool bUserDefinedOutPath = false;
    bool bSilent = false;
    bool bDumpSfxArchive = false;
    bool bExecuteWait = false;

    // Parse the Commandline
    try
    {
        QCommandLineParser parser;
        parser.setApplicationDescription("Archive Extractor");
        parser.addHelpOption();
        parser.addVersionOption();

        // Title
        QCommandLineOption titleOption(QStringList() << "T" << "title",
                                       QCoreApplication::translate("main", "Window Title"),
                                       QCoreApplication::translate("main", "title"));
        titleOption.setDefaultValue("Archive Extractor");
        parser.addOption(titleOption);

        // Time-out
        QCommandLineOption timeoutOption(QStringList() << "t" << "timeout",
                                   QCoreApplication::translate("main", "Time-Out in seconds"),
                                   QCoreApplication::translate("main", "#"));
        timeoutOption.setDefaultValue("0");
        parser.addOption(timeoutOption);

        // Archive Path
        QCommandLineOption archivePathOption(QStringList() << "a" << "archive",
                                     QCoreApplication::translate("main", "Archive Path"),
                                     QCoreApplication::translate("main", "path"));
        archivePathOption.setDefaultValue("");
        parser.addOption(archivePathOption);

        // Out Path
        QCommandLineOption outPathOption(QStringList() << "o" << "out",
                                     QCoreApplication::translate("main", "Out Path"),
                                     QCoreApplication::translate("main", "path"));
        outPathOption.setDefaultValue("");
        parser.addOption(outPathOption);

        // Silent
        QCommandLineOption silentOption(QStringList() << "s" << "silent", QCoreApplication::translate("main", "Silent"));
        parser.addOption(silentOption);

        // URL
        QCommandLineOption urlOption(QStringList() << "url",
                                     QCoreApplication::translate("main", "Archive URL"),
                                     QCoreApplication::translate("main", "url"));
        urlOption.setDefaultValue("");
        parser.addOption(urlOption);

        // SFX Path
        QCommandLineOption sfxPathOption(QStringList() << "sfx",
                                     QCoreApplication::translate("main", "Self-Extracting Executible Path\nRequires Archive Path (-a <path>)"),
                                     QCoreApplication::translate("main", "path"));
        sfxPathOption.setDefaultValue("");
        parser.addOption(sfxPathOption);

        // Dump SFX
        QCommandLineOption dsfxOption(QStringList() << "dsfx", QCoreApplication::translate("main", "Dump Self-Extracting Executible"));
        parser.addOption(dsfxOption);

        // Execute Path
        QCommandLineOption executePathOption(QStringList() << "x" << "execute",
                                     QCoreApplication::translate("main", "Executible Path"),
                                     QCoreApplication::translate("main", "path"));
        executePathOption.setDefaultValue("");
        parser.addOption(executePathOption);

        // Execute Relative Path
        QCommandLineOption executeRelativePathOption(QStringList() << "xrelative",
                                     QCoreApplication::translate("main", "Executible Path, relative to Out Path (-o <path>)"),
                                     QCoreApplication::translate("main", "path"));
        executeRelativePathOption.setDefaultValue("");
        parser.addOption(executeRelativePathOption);

        // Execute Arguments
        QCommandLineOption executeArgsOption(QStringList() << "xarg",
                                     QCoreApplication::translate("main", "Executible Arguments"),
                                     QCoreApplication::translate("main", "string"));
        executeArgsOption.setDefaultValue("");
        parser.addOption(executeArgsOption);

        // Move From Path
        QCommandLineOption moveFromPath(QStringList() << "movefrom",
                                     QCoreApplication::translate("main", "Move From Path (relative path)"),
                                     QCoreApplication::translate("main", "path"));
        moveFromPath.setDefaultValue("");
        parser.addOption(moveFromPath);

        // Move To Path
        QCommandLineOption moveToPath(QStringList() << "moveto",
                                     QCoreApplication::translate("main", "Move To Path (relative path)"),
                                     QCoreApplication::translate("main", "path"));
        moveToPath.setDefaultValue("");
        parser.addOption(moveToPath);

        // Execute Wait
        QCommandLineOption executeWaitOption(QStringList() << "xwait",
                                     QCoreApplication::translate("main", "Wait for Execution to finish"));
        parser.addOption(executeWaitOption);

        // License
        QCommandLineOption showLicense("license", QCoreApplication::translate("main", "Display license information."));
        parser.addOption(showLicense);

        parser.process(app);
        if(parser.isSet(showLicense))
        {
            qInfo("");
            qInfo("Copyright (C) 2016 TurningTechnologies, LLC.");
            qInfo("Contact: https://www.turningtechnologies.com/about-turning-technologies/contact-information");
            qInfo("");
            qInfo("This program is free software: you can redistribute it and/or modify");
            qInfo("it under the terms of the GNU General Public License as published by");
            qInfo("the Free Software Foundation, either version 3 of the License, or");
            qInfo("(at your option) any later version.");
            qInfo("");
            qInfo("This program is distributed in the hope that it will be useful,");
            qInfo("but WITHOUT ANY WARRANTY; without even the implied warranty of");
            qInfo("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
            qInfo("GNU General Public License for more details.");
            qInfo("");
            qInfo("You should have received a copy of the GNU General Public License");
            qInfo("along with this program.  If not, see <http://www.gnu.org/licenses/>.");
            qInfo("");
            return 0;
        }
        if(parser.isSet(titleOption))
        {
            sTitle = parser.value(titleOption);
        }
        if(parser.isSet(archivePathOption))
        {
            sArchivePath = parser.value(archivePathOption);
        }
        if(parser.isSet(outPathOption))
        {
            sOutPath = parser.value(outPathOption);
            bUserDefinedOutPath = true;
        }
        if(parser.isSet(silentOption))
        {
            bSilent = true;
        }
        if(parser.isSet(urlOption))
        {
            sArchiveURL = parser.value(urlOption);
        }
        if(parser.isSet(sfxPathOption))
        {
            sSfxPath = parser.value(sfxPathOption);
        }
        if(parser.isSet(dsfxOption))
        {
            bDumpSfxArchive = true;
        }
        if(parser.isSet(executePathOption))
        {
            sExecutePath = parser.value(executePathOption);
        }
        if(parser.isSet(executeRelativePathOption))
        {
            sExecuteRelativePath = parser.value(executeRelativePathOption);
        }
        if(parser.isSet(executeArgsOption))
        {
            sExecuteArgs = parser.value(executeArgsOption);
        }
        if(parser.isSet(executeWaitOption))
        {
            bExecuteWait = true;
        }
        if(parser.isSet(moveFromPath))
        {
            sMoveFromPath = parser.value(moveFromPath);
        }
        if(parser.isSet(moveToPath))
        {
            sMoveToPath = parser.value(moveToPath);
        }
    }
    catch(...)
    {
        qCritical("caught error: Parsing Commandline...");
    }

    // Move first...
    MovePath(sMoveFromPath, sMoveToPath);

    if(!sSfxPath.isEmpty())
    {
        if(!sArchivePath.isEmpty())
        {
            CreateSFX(sSfxPath, sAppPath, sArchivePath);
        }
        else if(!sArchiveURL.isEmpty())
        {
            QFile file(QDir().tempPath() + "/archive.temp");
            if(file.exists())
            {
                file.remove();
            }
            Download(sArchiveURL, file.fileName());
            if(file.exists())
            {
                CreateSFX(sSfxPath, sAppPath, file.fileName());
                file.remove();
            }
        }
        else
        {
            qInfo("Error > Missing Required Archive Path (-a <path>) or Archive URL (--url <url>)!");
            return -1;
        }
    }
    else if(bDumpSfxArchive)
    {
        if(!sArchivePath.isEmpty())
        {
            QFile fArchive(sArchivePath);
            if(fArchive.exists())
            {
                fArchive.remove();
            }
            QFile fApp(sAppPath);
            fApp.copy(sArchivePath);
            // Strip SFX stub from the archive
            LaunchApp("/usr/bin/zip", "-J \"" + sArchivePath + "\"", true);
        }
        else
        {
            qInfo("Error > Missing Required Archive Path (-a <path>)");
            return -1;
        }
    }
    else
    {
        // Get Output Location...
        if ((!bSilent) && (!bUserDefinedOutPath))
        {
            QFileDialog fileDlg;
            fileDlg.setWindowTitle(sTitle);
            fileDlg.setOption(QFileDialog::ShowDirsOnly, true);
            fileDlg.setOption(QFileDialog::DontConfirmOverwrite, true);
            //fileDlg.setOption(QFileDialog::DontUseNativeDialog, true);
            fileDlg.setOption(QFileDialog::HideNameFilterDetails, true);
            fileDlg.setDirectory(sOutPath);
            fileDlg.setWindowModality(Qt::WindowModal);
            if(fileDlg.exec())
            {
                if(!fileDlg.selectedFiles().isEmpty())
                {
                    sOutPath = fileDlg.selectedFiles()[0];
                }
            }
            else
            {
                return -1;
            }
        }

        if(!bSilent)
        {
            // Initialize the Progress Dialog
            m_pProgressDlg = new QProgressDialog();
            m_pProgressDlg->setWindowTitle(sTitle);
            m_pProgressDlg->setAutoReset(true);
            m_pProgressDlg->setAutoClose(false);
            m_pProgressDlg->setMaximum(101);
            m_pProgressDlg->setMinimum(0);
            m_pProgressDlg->setValue(1);
            m_pProgressDlg->setWindowModality(Qt::WindowModal);
            m_pProgressDlg->setLabelText(sOutPath);
        }
        if(!sArchivePath.isEmpty())
        {
            Unzip(sArchivePath, sOutPath);
        }
        else if(!sArchiveURL.isEmpty())
        {
            QFile file(QDir().tempPath() + "/archive.temp");
            if(file.exists())
            {
                file.remove();
            }
            Download(sArchiveURL, file.fileName());
            if(file.exists())
            {
                Unzip(file.fileName(), sOutPath);
                file.remove();
            }
        }
        else
        {
            // extract bundled archive
            QFile file(QDir().tempPath() + "/archive.temp");
            if(file.exists())
            {
                file.remove();
            }
            QFile fApp(sAppPath);
            fApp.copy(file.fileName());
            if(file.exists())
            {
                // Strip SFX stub from the archive
                LaunchApp("/usr/bin/zip", "-J \"" + file.fileName() + "\"", true);
                Unzip(file.fileName(), sOutPath);
                file.remove();
            }
        }

        if(!sExecutePath.isEmpty())
        {
            LaunchApp(sExecutePath, sExecuteArgs, false);
        }
        if(!sExecuteRelativePath.isEmpty())
        {
            LaunchApp(sOutPath + "/" + sExecuteRelativePath, sExecuteArgs, false);
        }

        if(m_pProgressDlg != NULL)
        {
            m_pProgressDlg->close();
            delete(m_pProgressDlg);
        }
    }
}

void Download(QString sURL, QString sDestination)
{
    if((!sURL.isEmpty())&&(!sDestination.isEmpty()))
    {
        struct timeval tvTime;
        struct timeval tvCurrent;
        gettimeofday(&tvTime, NULL);
        if(m_pProgressDlg != NULL)
        {
            m_pProgressDlg->show();
            m_pProgressDlg->repaint();
            m_pProgressDlg->activateWindow();
            m_pProgressDlg->update();
            QThread::msleep(50);
        }
        QString command = "/usr/bin/curl -so \"" + sDestination + "\" " + sURL;
        QProcess process;
        process.start(command);
        process.waitForStarted();
        int nProcessID = process.pid();
        while(!process.waitForFinished(12))
        {
            if(m_pProgressDlg != NULL)
            {
                if(m_pProgressDlg->wasCanceled())
                {
                    process.kill();
                    process.terminate();
                    break;
                }
                if(m_pProgressDlg->value() < 100)
                {
                    // increment the progress bar
                    m_pProgressDlg->setValue(m_pProgressDlg->value() + 1);
                }
                else
                {
                    // reset the progress bar
                    m_pProgressDlg->setValue(1);
                };
            }
            gettimeofday(&tvCurrent, NULL);
            if((tvCurrent.tv_sec - tvTime.tv_sec) > 2)
            {
                if(!ActivePID(nProcessID, "curl"))
                {
                    break;
                }
                gettimeofday(&tvTime, NULL);
            }
            QThread::msleep(12);
        }
    }
}

void Unzip(QString sArchive, QString sDestination)
{
    if((!sArchive.isEmpty())&&(!sDestination.isEmpty()))
    {
        struct timeval tvTime;
        struct timeval tvCurrent;
        gettimeofday(&tvTime, NULL);
        if(m_pProgressDlg != NULL)
        {
            m_pProgressDlg->show();
            m_pProgressDlg->repaint();
            m_pProgressDlg->activateWindow();
            m_pProgressDlg->update();
            QThread::msleep(50);
        }
        //QString command = "/usr/bin/unzip -qq -o \"" + sArchive + "\" -d \"" + sDestination + "\"";
        // use ditto to preserve resource forks
        QString command = "/usr/bin/ditto -xk \"" + sArchive + "\" \"" + sDestination + "\"";
        QProcess process;
        process.start(command);
        process.waitForStarted();
        int nProcessID = process.pid();
        while(!process.waitForFinished(12))
        {
            if(m_pProgressDlg != NULL)
            {
                if(m_pProgressDlg->wasCanceled())
                {
                    process.kill();
                    process.terminate();
                    break;
                }
                if(m_pProgressDlg->value() < 100)
                {
                    // increment the progress bar
                    m_pProgressDlg->setValue(m_pProgressDlg->value() + 1);
                }
                else
                {
                    // reset the progress bar
                    m_pProgressDlg->setValue(1);
                };
            }
            gettimeofday(&tvCurrent, NULL);
            if((tvCurrent.tv_sec - tvTime.tv_sec) > 2)
            {
                //if(!ActivePID(nProcessID, "unzip"))
                if(!ActivePID(nProcessID, "ditto"))
                {
                    break;
                }
                gettimeofday(&tvTime, NULL);
            }
            QThread::msleep(12);
        }
    }
}

void CreateSFX(QString sSfxPath, QString sAppPath, QString sArchivePath)
{
    QString command = "/bin/bash -c \"cat '" + sAppPath + "' '" + sArchivePath + "' > '" + sSfxPath + "'\"";
    QProcess process;
    process.start(command);
    process.waitForFinished();
    QString stdout = process.readAllStandardOutput();
    QString stderr = process.readAllStandardError();
    command = "/usr/bin/zip -A \"" + sSfxPath + "\"";
    process.start(command);
    process.waitForFinished();
    stdout = process.readAllStandardOutput();
    stderr = process.readAllStandardError();
    command = "/bin/chmod +x \"" + sSfxPath + "\"";
    process.start(command);
    process.waitForFinished();
    stdout = process.readAllStandardOutput();
    stderr = process.readAllStandardError();
}

bool ActivePID(int nPID, QString sAppName)
{
    QString command = "/usr/bin/pgrep \"" + sAppName + "\"";
    QProcess process;
    process.start(command);
    process.waitForFinished();
    QString stdout = process.readAllStandardOutput();
    QString stderr = process.readAllStandardError();
    return stdout.contains(QString::number(nPID));
}

void LaunchApp(QString sAppPath, QString sAppArg, bool bWait)
{
    QString command = "\"" + sAppPath + "\" " + sAppArg;
    QProcess process;
    if(bWait)
    {
        process.execute(command);
        /*
        process.start(command);
        process.waitForFinished();
        QByteArray stdout = process.readAllStandardOutput();
        QByteArray stderr = process.readAllStandardError();
        if(!stdout.isEmpty())
        {
            qInfo(stdout);
        }
        if(!stderr.isEmpty())
        {
            qInfo(stderr);
        }
        */
    }
    else
    {
        process.startDetached(command);
    }
}

void MovePath(QString sFrom, QString sTo)
{
    if ((!sFrom.isNull()) && (!sTo.isNull()))
    {
        if(QFile::exists(QDir::cleanPath(sFrom)))
        {
            if(!QFile::exists(QDir::cleanPath(sTo)))
            {
                QDir().mkpath(QDir::cleanPath(sTo + QDir::separator() + ".."));
                LaunchApp("/bin/mv", "-n \"" + QDir::cleanPath(sFrom) + "\" \"" + QDir::cleanPath(sTo) + "\"", true);
            }
        }
    }
}
