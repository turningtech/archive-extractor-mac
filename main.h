#ifndef MAIN_H
#define MAIN_H

#include <QtConcurrent>
#include <sys/time.h>
#include <QtWidgets>

#define VERSION "1.0.0.101"

static bool m_bExiting = false;
QProgressDialog *m_pProgressDlg = NULL;

void Download(QString sURL, QString sDestination);
void Unzip(QString sArchive, QString sDestination);
void CreateSFX(QString sSfxPath, QString sAppPath, QString sArchivePath);
bool ActivePID(int nPID, QString sAppName);
void LaunchApp(QString sAppPath, QString sAppArg, bool bWait);
void MovePath(QString sFrom, QString sTo);

#endif // MAIN_H


